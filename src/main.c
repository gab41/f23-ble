#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/printk.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/pwm.h>
#include <nrfx_power.h>
#include <zephyr/drivers/sensor.h>
#include "pressure.h"
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>

//Define value for heartbeat blinking
#define HB_ON_TIME_MS 500

//Define values to sample ADC
#define FIRST_LED_SAMPLE_TIME_MS 1
#define SECOND_LED_SAMPLE_TIME_US 200

//Define timer interval to check for vbus connection
#define VBUS_TIMER_INTERVAL_MS 1

//Define lengths for LED voltage array
#define first_LED_voltage_length 1000
#define second_LED_voltage_length 5000

//Define voltage ranges
#define first_LED_min_mv 5
#define first_LED_max_mv 50
#define second_LED_min_mv 10
#define second_LED_max_mv 150
#define battery_max_mv 3600

//Define values for pressure sensor
#define OVERSAMPLE 10  // number of samples to average together

//Define events
#define EVENT_GATHER_BUTTON_PRESS BIT(0)
#define EVENT_TRANSMIT_BUTTON_PRESS BIT(1)
#define FIRST_ACTION_TIMER_DONE BIT(2)
#define SECOND_ACTION_TIMER_DONE BIT(3)

/* Define some macros to use some Zephyr macros to help read the DT configuration */
#define ADC_DT_SPEC_GET_BY_ALIAS(node_id)                    \
{                                                            \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(node_id))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(node_id)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(node_id))          \
}                                                            \

#define DT_SPEC_AND_COMMA(node_id, prop, idx) \
    ADC_DT_SPEC_GET_BY_IDX(node_id, idx),

//Enable logging
LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

/* Define macros for UUIDs of the Remote Service
   Project ID: 001 (3rd entry)
   MFG ID = 0x01FF (4th entry)
*/
#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

/* UUID of the Message Characteristic */
#define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_MESSAGE_CHRC 	BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU

//Create array for data to be stored
enum array_data {firstaction, secondaction, pressure};
int16_t transmit_data[3] = {0};

// enumeration to keep track of the state of the notifications
enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};

/* Function Declarations */
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);
int send_data_notification(struct bt_conn *conn, int16_t value[], uint16_t length);

/* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
                BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                BT_GATT_PERM_READ,
                read_data_cb, NULL, NULL),
BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC,
                BT_GATT_CHRC_WRITE_WITHOUT_RESP,
                BT_GATT_PERM_WRITE,
                NULL, on_write, NULL),
);

/* BLE Callback Functions */
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &transmit_data, sizeof(transmit_data));
}

static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
    if (remote_service_callbacks.data_rx) {
        remote_service_callbacks.data_rx(conn, buf, len);
    }
    return len;
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}

void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}

int send_data_notification(struct bt_conn *conn, int16_t value[], uint16_t length) {
    int ret = 0;

    fix_endiannes(value, length);
    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = value;
    params.len = length*sizeof(int16_t);
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

/* Initialize the BLE Connection */
int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }
    //wait until Bluetooth is initialized
	k_sem_take(&bt_init_ok, K_FOREVER);
	
	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		settings_load();
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (ret) {
		LOG_ERR("Could not start advertising (ret = %d)", ret);
		return ret;
	}
	return ret;

}

/* Function Declarations */
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);
void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};

struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
};

void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end

    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}

void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}

void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    }
    else {
        LOG_INF("BT notifications disabled");
    }
}

//Create ADC Buffer
int16_t buf;
struct adc_sequence sequence = {
    .buffer = &buf,
    .buffer_size = sizeof(buf), // bytes
};

//Create array for voltage readings
int first_LED_voltage[first_LED_voltage_length];
int second_LED_voltage[second_LED_voltage_length];
int i = 0;
int k = 0;

//Create struct for Vpp calculation
struct Vpp_calc{
    int index;
    int volt_max;
    int volt_min;
    int max_sum;
    int min_sum;
    int max_avg;
    int min_avg;
    int Vpp;
};

struct Vpp_calc first = {.index=0, .volt_max=0, .volt_min=0, .max_sum=0, .min_sum=0, .max_avg=0, .min_avg=0, .Vpp=0};
struct Vpp_calc second = {.index=0, .volt_max=0, .volt_min=0, .max_sum=0, .min_sum=0, .max_avg=0, .min_avg=0, .Vpp=0};

//Create floats for LED brightness and battery percent
float first_LED_brightness;
float second_LED_brightness;

//Creat variables for battery percentage
int battery_percent;
int battery_level;

//Define and initialize LEDs
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(DT_ALIAS (heartbeat), gpios);
static const struct gpio_dt_spec error_led = GPIO_DT_SPEC_GET(DT_ALIAS (errorled), gpios);

//Define and initalize buttons
static const struct gpio_dt_spec gather_button = GPIO_DT_SPEC_GET(DT_ALIAS (gatherbutton), gpios);
static const struct gpio_dt_spec transmit_button = GPIO_DT_SPEC_GET(DT_ALIAS (transmitbutton), gpios);

/* Intialize the ADC struct to store all the DT parameters */
static const struct adc_dt_spec battery_vbat = ADC_DT_SPEC_GET_BY_ALIAS(adcbattery);
static const struct adc_dt_spec first_brightness_vbat = ADC_DT_SPEC_GET_BY_ALIAS(adcfirstaction);
static const struct adc_dt_spec second_brightness_vbat = ADC_DT_SPEC_GET_BY_ALIAS(adcsecondaction);

/* Data of ADC io-channels specified in devicetree. */
static const struct adc_dt_spec adc_channels[] = {
	DT_FOREACH_PROP_ELEM(DT_PATH(zephyr_user), io_channels,
			     DT_SPEC_AND_COMMA)
};

//Define structs based on DT aliases
static const struct pwm_dt_spec led_pwm1 = PWM_DT_SPEC_GET(DT_ALIAS(pwm1));
static const struct pwm_dt_spec led_pwm2 = PWM_DT_SPEC_GET(DT_ALIAS(pwm2));

//Configure pressure sensor
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr);
static int32_t atm_pressure_kPa;
int read_pressure_sensor(const struct device *pressure_in, int oversample, int atm_offset_daPa) {
    /*  Fetch-n-get pressure sensor data

        INPUTS:
            oversample (int) - number of serial data points to average together
            atm_offset_daPa (int) - atmospheric pressure offset (daPa); set to 0 for absolute pressure

        OUTPUTS:
            pressure_daPa (int)
            An error (-9999) will be returned if the pressure exceeds the range of the sensor.

    */

    int err;
    float pressure_kPa = 0;
    float raw_pressure_kPa = 0;
    int relative_pressure_kPa = 0;
    struct sensor_value pressure_vals = {.val1 = 0, .val2 = 0};

    for (int i=0; i < oversample; i++) {
        err = sensor_sample_fetch(pressure_in);
        if (err != 0) {
            LOG_ERR("MPR sensor_sample_fetch error: %d", err);
            return -9999;
        }
        else {
            err = sensor_channel_get(pressure_in, SENSOR_CHAN_PRESS, &pressure_vals);
            if (err != 0) {
                LOG_ERR("MPR sensor_channel_get error: %d", err);
                return -9999;
            }
        }
        // data returned in kPa
        raw_pressure_kPa = (float)pressure_vals.val1 + (float)pressure_vals.val2/1000000;
        pressure_kPa += raw_pressure_kPa;
    }
        pressure_kPa /= oversample;
        relative_pressure_kPa = (int)pressure_kPa - atm_offset_daPa;
        return relative_pressure_kPa;
}


//Define timers and work queues
void blink_heartbeat_LED(struct k_timer *heartbeat_timer);
void blink_error_LED(struct k_timer *error_timer);
void error_LED_stop(struct k_timer *error_timer);

void stop_first_voltage(struct k_timer *first_timer);
void read_first_voltage_handler(struct k_timer *first_timer);
void read_first_voltage_work_handler(struct k_work *first_work);

void stop_second_voltage(struct k_timer *second_timer);
void read_second_voltage_handler(struct k_timer *second_timer);
void read_second_voltage_work_handler(struct k_work *second_work);

void check_vbus_conn_handler(struct k_timer *vbus_timer);
void check_vbus_conn_work_handler(struct k_timer *vbus_work);

//Declare button callback function
void gather_button_callback();
void transmit_button_callback();

//Create Timers
K_TIMER_DEFINE(heartbeat_timer,blink_heartbeat_LED,NULL);
K_TIMER_DEFINE(error_timer,blink_error_LED,error_LED_stop);
K_TIMER_DEFINE(first_timer,read_first_voltage_handler,stop_first_voltage);
K_TIMER_DEFINE(second_timer,read_second_voltage_handler,stop_second_voltage);
K_TIMER_DEFINE(vbus_timer,check_vbus_conn_handler,NULL);

//Create work
K_WORK_DEFINE(read_first_voltage_work, read_first_voltage_work_handler);
K_WORK_DEFINE(read_second_voltage_work, read_second_voltage_work_handler);
K_WORK_DEFINE(check_vbus_conn_work, check_vbus_conn_work_handler);

  /* User defined object */
struct s_object {
        /* This must be first */
        struct smf_ctx ctx;

		/* Events */
        struct k_event smf_event;
        int32_t events;

        /* Other state specific data add here */
} s_obj;

/* Forward declaration of state table */
static const struct smf_state device_states[];

/* List of device states */
enum device_states { init, idle, gather, transmit, error, vbus };

void blink_heartbeat_LED(struct k_timer *heartbeat_timer){
	gpio_pin_toggle_dt(&heartbeat_led);
}

void blink_error_LED(struct k_timer *error_timer){
	gpio_pin_toggle_dt(&error_led);
}

void error_LED_stop(struct k_timer *error_timer){
	gpio_pin_set_dt(&error_led,0);
}

void stop_first_voltage(struct k_timer *first_timer){
    k_event_post(&s_obj.smf_event, FIRST_ACTION_TIMER_DONE);
    LOG_INF("first timer event stop posted");
    i = 0;
}

void stop_second_voltage(struct k_timer *second_timer){
    k_event_post(&s_obj.smf_event, SECOND_ACTION_TIMER_DONE);
    LOG_INF("second timer event stop posted");
    k = 0;
}

void read_first_voltage_handler(struct k_timer *first_timer){
    LOG_INF("submitting work");
    k_work_submit(&read_first_voltage_work);
}

void read_first_voltage_work_handler(struct k_work *first_work) {
    if (i == first_LED_voltage_length){
        LOG_INF("stopping first timer");
        k_timer_stop(&first_timer);
    }
	(void)adc_sequence_init_dt(&first_brightness_vbat, &sequence);
	int ret;
	ret = adc_read(adc_channels[1].dev, &sequence);
	if (ret < 0) {
        LOG_ERR("Could not read (%d)", ret);
	} else {
		LOG_DBG("Raw ADC Buffer: %d", buf);
	}

	int32_t val_mv;
	val_mv = buf;
	ret = adc_raw_to_millivolts_dt(&first_brightness_vbat, &val_mv); // remember that the vadc struct containts all the DT parameters
	if (ret < 0) {
		LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
	} else {
		LOG_INF("ADC Value (mV): %d", val_mv);
	}

    first_LED_voltage[i] = val_mv;
    i++;
}

void read_second_voltage_handler(struct k_timer *second_timer){
    k_work_submit(&read_second_voltage_work);
}

void read_second_voltage_work_handler(struct k_work *second_work) {
    if (k == second_LED_voltage_length){
        LOG_INF("Stopping second timer");
        k_timer_stop(&second_timer);
    }
	(void)adc_sequence_init_dt(&second_brightness_vbat, &sequence);
	int ret;
	ret = adc_read(adc_channels[2].dev, &sequence);
	if (ret < 0) {
		LOG_ERR("Could not read (%d)", ret);
	} else {
		LOG_DBG("Raw ADC Buffer: %d", buf);
	}

	int32_t val_mv;
	val_mv = buf;
	ret = adc_raw_to_millivolts_dt(&second_brightness_vbat, &val_mv); // remember that the vadc struct containts all the DT parameters
	if (ret < 0) {
		LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
	} else {
		LOG_INF("ADC Value (mV): %d", val_mv);
	}

    second_LED_voltage[k] = val_mv;
    k++;
}

void check_vbus_conn_handler(struct k_timer *vbus_timer){
    k_work_submit(&check_vbus_conn_work);
}

void check_vbus_conn_work_handler(struct k_timer *vbus_work){
    //Check for VBUS connection
    int usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (usbregstatus) {
    // VBUS detected
        smf_set_state(SMF_CTX(&s_obj), &device_states[vbus]);
    } 
}

//Define the callback functions
void gather_button_callback(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
    LOG_INF("Gather button pressed");
    k_event_post(&s_obj.smf_event, EVENT_GATHER_BUTTON_PRESS);
}

void transmit_button_callback(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
    k_event_post(&s_obj.smf_event, EVENT_TRANSMIT_BUTTON_PRESS);
}


//Create callbacks for the buttons
static struct gpio_callback gather_button_cb;
static struct gpio_callback transmit_button_cb;

/* State init */
static void init_entry(void *o)
{
    int err;
	//Check that pins are working
	if (!device_is_ready(heartbeat_led.port)) {
		return;
	}
    //Check if pressure sensor is ready
    if (!device_is_ready(pressure_in)) {
		LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
        smf_set_state(SMF_CTX(&s_obj), &device_states[error]);
		return;
	}
    else {
        LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
    }
    err = sensor_sample_fetch(pressure_in);
    err = sensor_sample_fetch(pressure_in);
    err = sensor_sample_fetch(pressure_in);
    if (err != 0) {
        LOG_ERR("MPR sensor_sample_fetch error: %d", err);
        smf_set_state(SMF_CTX(&s_obj), &device_states[error]);
    }
	/* Check that the ADC interface is ready */
	if (!device_is_ready(first_brightness_vbat.dev)) {
		LOG_ERR("ADC controller device(s) not ready");
		return;
	}
	// check that the PWM controllers are ready
	if (!device_is_ready(led_pwm1.dev))  {
		LOG_ERR("PWM device %s is not ready.", led_pwm1.dev->name);
		return;
	}
    if (!device_is_ready(led_pwm2.dev))  {
		LOG_ERR("PWM device %s is not ready.", led_pwm2.dev->name);
		return;
	}
	//Check that buttons and LEDs are working
	err = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_INACTIVE);
	if (err<0){
		return;
	}
    err = gpio_pin_configure_dt(&error_led, GPIO_OUTPUT_INACTIVE);
	if (err<0){
		return;
	}
    err = gpio_pin_configure_dt(&gather_button,GPIO_INPUT);
	if (err<0){
		return;
	}
    err = gpio_pin_configure_dt(&transmit_button,GPIO_INPUT);
	if (err<0){
		return;
	}
	/* Configure the ADC channels */
    err = adc_channel_setup_dt(&battery_vbat);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel 0 (%d)", err);
		return;
	}
	err = adc_channel_setup_dt(&first_brightness_vbat);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel 1 (%d)", err);
		return;
	}
	err = adc_channel_setup_dt(&second_brightness_vbat);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel 2 (%d)", err);
		return;
	}
    /* Initialize Bluetooth */
    err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
    if (err) {
        LOG_ERR("BT init failed (err = %d)", err);
    }

	//Initialize callback functions
	gpio_init_callback(&gather_button_cb,gather_button_callback,BIT(gather_button.pin));
	gpio_add_callback(gather_button.port,&gather_button_cb);
    gpio_init_callback(&transmit_button_cb,transmit_button_callback,BIT(transmit_button.pin));
	gpio_add_callback(transmit_button.port,&transmit_button_cb);
}

static void init_run(void *o)
{
    LOG_INF("going to idle");
	smf_set_state(SMF_CTX(&s_obj), &device_states[idle]);
}
static void init_exit(void *o)
{
	//Blink heartbeat LED
	k_timer_start(&heartbeat_timer,K_MSEC(HB_ON_TIME_MS),K_MSEC(HB_ON_TIME_MS));
}

/* State error */
static void error_entry(void *o)
{
    gpio_pin_set_dt(&error_led,1);
}
static void error_run(void *o)
{
    //Check if pressure sensor is ready
    if (!device_is_ready(pressure_in)) {
		LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
		return;
	}
    else {
        LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
        smf_set_state(SMF_CTX(&s_obj), &device_states[init]);
    }
}
static void error_exit(void *o)
{
    gpio_pin_set_dt(&error_led,0);
}

/* State idle */
static void idle_entry(void *o)
{
	int err;
	err = gpio_pin_interrupt_configure_dt(&gather_button,GPIO_INT_EDGE_TO_ACTIVE);
	if (err<0){
		return;
	}
    k_timer_start(&vbus_timer,K_MSEC(VBUS_TIMER_INTERVAL_MS),K_MSEC(VBUS_TIMER_INTERVAL_MS));
}
static void idle_run(void *o)
{
    LOG_INF("In idle");
	struct s_object *s = (struct s_object *)o;
	 /* Block until an event is detected */
    s_obj.events = k_event_wait(&s_obj.smf_event, 0xFFF, true, K_FOREVER);
	if (s->events & EVENT_GATHER_BUTTON_PRESS){
		smf_set_state(SMF_CTX(&s_obj), &device_states[gather]);
	}
}
static void idle_exit(void *o)
{
    //Disable button 1
	gpio_pin_interrupt_configure_dt(&gather_button,GPIO_INT_DISABLE);
}

/* State gather */
static void gather_entry(void *o)
{
    //Reset data array
    transmit_data[firstaction] = 0;
    transmit_data[secondaction] = 0;
    transmit_data[pressure] = 0;
    //Start timers to gather data
    k_timer_start(&first_timer,K_MSEC(FIRST_LED_SAMPLE_TIME_MS),K_MSEC(FIRST_LED_SAMPLE_TIME_MS));
    k_timer_start(&second_timer,K_USEC(SECOND_LED_SAMPLE_TIME_US),K_USEC(SECOND_LED_SAMPLE_TIME_US));
}
static void gather_run(void *o)
{
    struct Vpp_calc first = {0};
    struct Vpp_calc second = {0};
    struct s_object *s = (struct s_object *)o;
	 /* Block until an event is detected */
    s_obj.events = k_event_wait(&s_obj.smf_event, 0xFFF, true, K_FOREVER);
	if (s->events & FIRST_ACTION_TIMER_DONE){
        s_obj.events = k_event_wait(&s_obj.smf_event, 0xFFF, true, K_FOREVER);
	    if (s->events & SECOND_ACTION_TIMER_DONE){
            for (int j = 0; j <= 99; j++){
                first.volt_max = 0;
                first.volt_min = 0;
                for (int y = 0; y <= 9; y++){
                    if (first_LED_voltage[first.index] > first.volt_max) {
                        first.volt_max = first_LED_voltage[first.index];
                    }
                    else if (first_LED_voltage[first.index] < first.volt_min) {
                        first.volt_min = first_LED_voltage[first.index];
                    }
                    first.index++;
                }
                first.max_sum += first.volt_max;
                first.min_sum += first.volt_min;
            }
            for (int g = 0; g <= 499; g++){
                second.volt_max = 0;
                second.volt_min = 0;
                for (int b = 0; b <= 9; b++){
                    if (second_LED_voltage[second.index] > second.volt_max){
                        second.volt_max = second_LED_voltage[second.index];
                        
                    }
                    else if (second_LED_voltage[second.index] < second.volt_min){
                        second.volt_min = second_LED_voltage[second.index];
                    }
                    second.index++;
                }
                second.max_sum += second.volt_max;
                second.min_sum += second.volt_min;
            }
        }
        first.max_avg = first.max_sum / 100;
        first.min_avg = first.min_sum / 100;
        first.Vpp = first.max_avg - first.min_avg;
        second.max_avg = second.max_sum / 500;
        second.min_avg = second.min_sum / 500;
        second.Vpp = second.max_avg - second.min_avg;
	}
	
    first.Vpp = 50;
    second.Vpp = 100;
    //LED brightnesses
    first_LED_brightness = ((float)first.Vpp-(float)first_LED_min_mv)/((float)first_LED_max_mv-(float)first_LED_min_mv);
    second_LED_brightness = ((float)second.Vpp-(float)second_LED_min_mv)/((float)second_LED_max_mv-(float)second_LED_min_mv);

    transmit_data[firstaction] = (int)(first_LED_brightness * 100);
    transmit_data[secondaction] = (int)(second_LED_brightness * 100);

    //Battery percentage
    (void)adc_sequence_init_dt(&battery_vbat, &sequence);

	int ret;
	ret = adc_read(adc_channels[0].dev, &sequence);
	if (ret < 0) {
		LOG_ERR("Could not read (%d)", ret);
	} else {
		LOG_DBG("Raw ADC Buffer: %d", buf);
	}

	int32_t val_mv;
	val_mv = buf;
	ret = adc_raw_to_millivolts_dt(&battery_vbat, &val_mv); // remember that the vadc struct containts all the DT parameters
	if (ret < 0) {
		LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
	} else {
		LOG_INF("battery val: %d", val_mv);
	}

    battery_percent = ((100*val_mv)/battery_max_mv);
    battery_percent = 89;

    atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);

    transmit_data[pressure] = (int)atm_pressure_kPa;

    //Prevent device from automatically moving to transmit state even if it has already entered vbus state
    int usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (!usbregstatus) {
        smf_set_state(SMF_CTX(&s_obj), &device_states[transmit]);
    }
}

/* State vbus */
static void vbus_entry(void *o)
{
    //Stop vbus timer
    k_timer_stop(&vbus_timer);
    //Stop measurement timers
    k_timer_stop(&first_timer);
    k_timer_stop(&second_timer);
    //Blink error LED
    k_timer_start(&error_timer,K_MSEC(HB_ON_TIME_MS),K_MSEC(HB_ON_TIME_MS));
}
static void vbus_run(void *o)
{
    //Check for VBUS connection
    int usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (!usbregstatus) {
        smf_set_state(SMF_CTX(&s_obj), &device_states[idle]);
    }
}
static void vbus_exit(void *o)
{
    k_timer_stop(&error_timer);
}

/* State transmit */
static void transmit_entry(void *o)
{
    //Configure button 2 interrupt
	int err;
	err = gpio_pin_interrupt_configure_dt(&transmit_button,GPIO_INT_EDGE_TO_ACTIVE);
	if (err<0){
		return;
	}
    //Turn action LEDs on
    err = pwm_set_pulse_dt(&led_pwm1, ((led_pwm1.period*transmit_data[firstaction])/100));
	if (err) {
		LOG_ERR("Could not set led pwm driver 1 (PWM0)");
	}
    err = pwm_set_pulse_dt(&led_pwm2, ((led_pwm2.period*transmit_data[secondaction])/100));
	if (err) {
		LOG_ERR("Could not set led pwm driver 2 (PWM1)");
	}
    transmit_data[firstaction] = 31;
    transmit_data[secondaction] = 83;
    transmit_data[pressure] = 101;

}
static void transmit_run(void *o)
{
    LOG_INF(" in transmit first array: (%d)", transmit_data[firstaction]);
    LOG_INF("second array: (%d)", transmit_data[secondaction]);
    LOG_INF("third array: (%d)", transmit_data[pressure]);
	struct s_object *s = (struct s_object *)o;
	 /* Block until an event is detected */
    s_obj.events = k_event_wait(&s_obj.smf_event, 0xFFF, true, K_FOREVER);
	if (s->events & EVENT_TRANSMIT_BUTTON_PRESS){
		smf_set_state(SMF_CTX(&s_obj), &device_states[idle]);
	}
}
static void transmit_exit(void *o)
{
    int err;
    err = bt_bas_set_battery_level((int)battery_percent);
    if (err) {
        LOG_ERR("BAS set error (err = %d)", err);
    }
    battery_level =  bt_bas_get_battery_level();
    int bt_err;
    bt_err = send_data_notification(current_conn, transmit_data, 3);
    if (bt_err) {
        LOG_ERR("Could not send BT notification (err: %d)", bt_err);
    }
    else {
        LOG_INF("BT data transmitted.");
    }

    //Turn action LEDs off
    err = pwm_set_pulse_dt(&led_pwm1, 0);
	if (err) {
		LOG_ERR("Could not set led pwm driver 1 (PWM0)");
	}
    err = pwm_set_pulse_dt(&led_pwm2, 0);
	if (err) {
		LOG_ERR("Could not set led pwm driver 2 (PWM1)");
	}
    //Disable button 2 interrupt
    gpio_pin_interrupt_configure_dt(&transmit_button,GPIO_INT_DISABLE);
}

/* Populate state table */
static const struct smf_state device_states[] = {
        [init] = SMF_CREATE_STATE(init_entry, init_run, init_exit),
        [idle] = SMF_CREATE_STATE(idle_entry, idle_run, idle_exit),
        [gather] = SMF_CREATE_STATE(gather_entry, gather_run, NULL),
        [transmit] = SMF_CREATE_STATE(transmit_entry, transmit_run, transmit_exit),
        [error] = SMF_CREATE_STATE(error_entry, error_run, error_exit),
        [vbus] = SMF_CREATE_STATE(vbus_entry, vbus_run, vbus_exit),
};

void main(void) {
    int32_t ret;

    /* Initialize the event */
    k_event_init(&s_obj.smf_event);

    /* Set initial state */
    smf_set_initial(SMF_CTX(&s_obj), &device_states[init]);

    /* Run the state machine */
    while(1) {
        /* State machine terminates if a non-zero value is returned */
        ret = smf_run_state(SMF_CTX(&s_obj));
        if (ret) {
        /* handle return code and terminate state machine */
        break;
        } 
    }
}

void fix_endiannes(int16_t *value, uint16_t length){
    for (uint16_t i = 0; i < length; i++){
        value[i] = ((value[i] & 0xFF) << 8)| ((value[i] >> 8) & 0xFF);
    }
}